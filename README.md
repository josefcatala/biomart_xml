# biomart_xml

## Biomart Ensembl xml query downloader

This script get as arguments a xml file with a biomart query and an output file name.
Available genomes: grch37 and grch38.

It is possible to define the output file with or without header by modifying the option in the query file: header="0" (not header) or header="1" (header).

`EXAMPLE: biomart_xml.py query.xml data_output.txt grch37`

*BioMart browser sessions running for more than 5 minutes are terminated*

### TO DO:

* Add alternative servers to chgr38 if main server is not working (uswest.ensembl.org, useast.ensembl.org, asia.ensembl.org)

### SOLVED:

* <SOLVED> The output don't have the header line. Why and how to fix it?
    * Not a code issue. Is an option in the query header="0" (not header) or header="1" (header)
* <SOLVED> Report/message when ensembl is not working. How to check it?. Change message.
    * When response.status_code != 200 an error is printed and file is not saved
* <SOLVED> Use a dictionary (a json in the future) to associate a reference genome name (input) to an URL. If not in the dict, display a KeyError
