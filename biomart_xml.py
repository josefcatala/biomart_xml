#!/usr/bin/env python
# coding: utf-8

# 20180313

# This script get as arguments a xml file with a biomart query and an output file name
# EXAMPLE: biomart_xml.py query.xml data_output.txt grch37

# SOLVED:
# * <SOLVED> The output don't have the header line. Why and how to fix it?
#   - Not a code issue. Is an option in the query header="0" (not header) or header="1" (header)
# * <SOLVED> Report/message when ensembl is not working. How to check it?. Change message.
#   - When response.status_code != 200 an error is printed and file is not saved
# * <SOLVED> Use a dictionary (a json in the future) to associate a reference
#            genome name (input) to an URL. If not in the dict, display a KeyError



import argparse
import re
import requests

parser = argparse.ArgumentParser(description = "This script download biomart data using a xml query. You can choose the genome of refence: grch37 or grch38. EXAMPLE: biomart_xml.py query.xml data_output.txt grch37")
parser.add_argument("input", help="Input file containing the xml query")
parser.add_argument("output", help="Output file with the result")
parser.add_argument("ref", help="Genome of reference: grch37 or grch38", choices=['grch37', 'grch38'])
# Save args in a dict
args = vars(parser.parse_args()) #To use argparse in jupyter notebook: https://stackoverflow.com/a/30665350

file_input = args["input"]
file_output = args["output"]
reference = args["ref"]

# For non argparse argument test
# file_input="cosa.xml"
# file_output="out.txt"
# reference = "grch37"


# Biomart URLs
ref_genomes = {"grch37":"http://grch37.ensembl.org/biomart/martservice?query=",
"grch38":"http://www.ensembl.org/biomart/martservice?query="}

with open(file_input, mode="r") as query:
    # Read the file
    query = query.read()
    # Clean lines
    query = query.strip("\n\t\r")
    # Remove spaces between ">" and "<"
    reg = re.compile(">\s+<")
    clean_query = reg.sub("><",query)


# Getting request
url = ref_genomes[reference] + clean_query
response = requests.get(url)


# Check status code
if response.status_code == requests.codes.ok:
    # Writing the file
    with open(file_output, mode="w") as output:
        output.write(response.text)
else:
    print("ERROR: It was not possible to get the data from ensembl.")
    print("Status code: {}".format(response.status_code))
    
